import com.testwork.callcenter.Main;
import com.testwork.callcenter.models.Call;
import com.testwork.callcenter.models.Company;
import com.testwork.callcenter.models.Employee;
import com.testwork.callcenter.models.Role;
import com.testwork.callcenter.utils.GeneralUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by renat on 22.03.16.
 */
public class CallcenterTest {
    private static Company company = new Company();
    private Comparator<Employee> employeeComparator =  new Comparator<Employee>() {
        public int compare(Employee e1, Employee e2) {
            //first who has less calls
            return e1.getReceivedCallsCount() - e2.getReceivedCallsCount();
        }
    };

    @BeforeClass
    public static void preparetion() {
        Company.ITryToHandleCall iTryToHandleCall = new Company.ITryToHandleCall() {
            public void tryToHandleCall(Role role) {
                System.out.println("tryind to handle call from queue ");

                company.tryToHandleFirstCallFromQueue(role);
            }
        };
        for (int i = 1; i <= Main.AGENT_COUNT; i++) {
            company.addAgent(new Employee(i, "Agent name "+ i, iTryToHandleCall));
        }

        company.setManager(new Employee(Main.AGENT_COUNT+1, "Manager", iTryToHandleCall, Role.MANAGER));
        company.setSupervisor(new Employee(Main.AGENT_COUNT+2, "Supervisor", iTryToHandleCall, Role.SUPERVISOR));
        Calendar calendar = Calendar.getInstance();
        for (int i = 0 ; i < Main.CALLS_COUNT; i++) {
            calendar.add(Calendar.SECOND, i);
            Employee employee = company.findCallHandler(new Call(i, calendar.getTime(), GeneralUtils.randInt(100, 999), Role.AGENT), true);
            if (employee == null) {
                System.out.println("Call added to queue");
                continue;
            }
            System.out.println("Call is taken by "+ employee);
        }

        for (int i = Main.CALLS_COUNT ; i < Main.MANAGER_CALLS_COUNT+Main.CALLS_COUNT; i++) {
            calendar.add(Calendar.SECOND, i);
            Employee employee = company.findCallHandler(new Call(i, calendar.getTime(), GeneralUtils.randInt(100, 999), Role.MANAGER), true);
            if (employee == null) {
                System.out.println("Manager Call added to queue");
                continue;
            }
            System.out.println("Manager Call is taken by "+ employee);
        }

        for (int i = Main.MANAGER_CALLS_COUNT+Main.CALLS_COUNT ; i < Main.MANAGER_CALLS_COUNT+Main.CALLS_COUNT + Main.SUPERVISOR_CALLS_COUNT; i++) {
            calendar.add(Calendar.SECOND, i);
            Employee employee = company.findCallHandler(new Call(i, calendar.getTime(), GeneralUtils.randInt(100, 999), Role.SUPERVISOR), true);
            if (employee == null) {
                System.out.println("Supervisor Call added to queue");
                continue;
            }
            System.out.println("Supervisor Call is taken by "+ employee);
        }

        while (company.getCallQueue().size() > 0) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    //Test for count to be equals answered calls and income calls
    @Test
    public void sizeCallTest() {
        assertEquals(Main.MANAGER_CALLS_COUNT+Main.CALLS_COUNT+Main.SUPERVISOR_CALLS_COUNT,
                company.getAnsweredCalls().size());

    }
    //Test for count calls by Roles
    @Test
    public void sizeCallsByRolesTest() {
        int agentCallCount = 0;
        for (Employee employee : company.getAgentList()) {
            agentCallCount+=employee.getReceivedCallsCount();
        }

        assertEquals(company.getManager().getReceivedCallsCount(), Main.MANAGER_CALLS_COUNT);
        assertEquals(company.getSupervisor().getReceivedCallsCount(), Main.SUPERVISOR_CALLS_COUNT);
        assertEquals(agentCallCount, Main.CALLS_COUNT);
    }
    //test to count delta for received calls by agent
    @Test
    public void deltaCount() {
        List<Employee> agentList = company.getAgentList();
        Collections.sort(agentList, employeeComparator);

        int firstNumberCalls = agentList.get(0).getReceivedCallsCount();
        int lastNumberCalls = agentList.get(agentList.size()-1).getReceivedCallsCount();
        assertTrue(Math.abs(firstNumberCalls - lastNumberCalls) < Main.CALLS_COUNT/4);
    }

}
