package com.testwork.callcenter;

import com.testwork.callcenter.models.Call;
import com.testwork.callcenter.models.Company;
import com.testwork.callcenter.models.Employee;
import com.testwork.callcenter.models.Role;
import com.testwork.callcenter.utils.GeneralUtils;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by renat on 21.03.16.
 */
public class Main {
    final public static int AGENT_COUNT = 10;
    final public static int CALLS_COUNT = 100;
    final public static int MANAGER_CALLS_COUNT = 10;
    final public static int SUPERVISOR_CALLS_COUNT = 10;

    public static void main(String[] args) {
        final Company company = new Company();
        Company.ITryToHandleCall iTryToHandleCall = new Company.ITryToHandleCall() {
            public void tryToHandleCall(Role role) {
                System.out.println("tryind to handle call from queue ");

                company.tryToHandleFirstCallFromQueue(role);
            }
        };
        for (int i = 1 ; i <= AGENT_COUNT; i++) {
            company.addAgent(new Employee(i, "Agent name "+ i, iTryToHandleCall));
        }

        company.setManager(new Employee(AGENT_COUNT+1, "Manager", iTryToHandleCall, Role.MANAGER));
        company.setSupervisor(new Employee(AGENT_COUNT+2, "Supervisor", iTryToHandleCall, Role.SUPERVISOR));
        Calendar calendar = Calendar.getInstance();
        for (int i = 0 ; i < CALLS_COUNT; i++) {
            calendar.add(Calendar.SECOND, i);
            Employee employee = company.findCallHandler(new Call(i, calendar.getTime(), GeneralUtils.randInt(100, 9990), Role.AGENT), true);
            if (employee == null) {
                System.out.println("Call added to queue");
                continue;
            }
            System.out.println("Call is taken by "+ employee);
        }

        for (int i = 0 ; i < MANAGER_CALLS_COUNT+0; i++) {
            calendar.add(Calendar.SECOND, i);
            Employee employee = company.findCallHandler(new Call(i, calendar.getTime(), GeneralUtils.randInt(100, 9990), Role.MANAGER), true);
            if (employee == null) {
                System.out.println("Manager Call added to queue");
                continue;
            }
            System.out.println("Manager Call is taken by "+ employee);
        }

        for (int i = 0 ; i < SUPERVISOR_CALLS_COUNT+0; i++) {
            calendar.add(Calendar.SECOND, i);
            Employee employee = company.findCallHandler(new Call(i, calendar.getTime(), GeneralUtils.randInt(100, 9990), Role.SUPERVISOR), true);
            if (employee == null) {
                System.out.println("Supervisor Call added to queue");
                continue;
            }
            System.out.println("Supervisor Call is taken by "+ employee);
        }


    }
}
