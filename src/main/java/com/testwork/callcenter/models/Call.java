package com.testwork.callcenter.models;

import java.util.Date;

/**
 * Created by renat on 20.03.16.
 */
public class Call {
    private long id;
    private Date date;
    private long duration;
    private Boolean answered = Boolean.FALSE;
    private Role role;

    public Call() {
    }

    public Call(long id, Date date, long duration) {
        this.id = id;
        this.date = date;
        this.duration = duration;
        this.role = Role.AGENT;
    }

    public Call(long id, Date date, long duration, Role role) {
        this.id = id;
        this.date = date;
        this.duration = duration;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public Boolean getAnswered() {
        return answered;
    }

    public void setAnswered(Boolean answered) {
        this.answered = answered;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Call{" +
                "id=" + id +
                ", date=" + date +
                ", duration=" + duration +
                ", answered=" + answered +
                ", role=" + role +
                '}';
    }
}
