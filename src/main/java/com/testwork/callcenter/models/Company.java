package com.testwork.callcenter.models;

import java.util.*;

/**
 * Created by renat on 20.03.16.
 */
public class Company {
    private Employee supervisor;
    private Employee manager;
    private List<Employee> agentList;
    private Queue<Call> callQueue = new ArrayDeque<Call>();
    private List<Call> answeredCalls = new ArrayList<Call>();
    private Comparator<Employee> employeeComparator =  new Comparator<Employee>() {
        public int compare(Employee e1, Employee e2) {
            //first who has less calls
            return e1.getReceivedCallsCount() - e2.getReceivedCallsCount();
        }
    };

    public Company() {
    }

    public Employee findCallHandler(Call call, Boolean needQueue) {
        switch (call.getRole()) {
            case AGENT:
                Collections.sort(agentList, employeeComparator);
                for (Employee agent : agentList) {
                    if (agent.canHandle()) {
                        return handleCall(agent, call);
                    }
                }
                break;
            case MANAGER:
                if (manager != null && manager.canHandle()) {
                    return handleCall(manager, call);
                } else if (supervisor != null && supervisor.canHandle()) {
                    return handleCall(supervisor, call);
                }
                break;
            case SUPERVISOR:
                if(supervisor != null && supervisor.canHandle()){
                    return handleCall(supervisor, call);
                }
                break;
        }

        if (needQueue) addToQueue(call);

        return null;
    }

    private Employee handleCall(Employee agent, Call call) {
        if (call.getAnswered()) return null;
        answeredCalls.add(call);
        removeFromQueue(call);
        return agent.handle(call);
    }

    private void addToQueue(Call call) {
        this.callQueue.add(call);
    }

    private void removeFromQueue(Call call) {
        if (callQueue != null) {
            if (callQueue.contains(call)) {
                callQueue.remove(call);
            }
        }
    }

    public void tryToHandleFirstCallFromQueue(Role role) {
        if (callQueue.size() > 0) {
            System.out.println("Now size of queue is " + callQueue.size());
            Call callFromQueue = getFirstFromQueue(role);
            if (callFromQueue == null || callFromQueue.getAnswered()) return;

            System.out.println("Call from queue is " + callFromQueue);
            try {
                findCallHandler(callFromQueue, false);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        } else {
            System.out.println("!!!!! Statistics: answered calls size: " + answeredCalls.size());
            String stat = "";
            for (Employee agent: agentList) {
                stat += agent.toString()+"\n";
            }
            stat += manager.toString()+"\n";
            stat += supervisor.toString()+"\n";

            System.out.println("!!!!! Statistics: "+ stat);
        }
    }

    private Call getFirstFromQueue(Role role) {
        for (Call call : callQueue) {
            if (role == call.getRole()) return call;
        }
        return null;
    }


    public Employee getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Employee supervisor) {
        this.supervisor = supervisor;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public List<Employee> getAgentList() {
        return agentList;
    }

    public void setAgentList(List<Employee> agentList) {
        this.agentList = agentList;
    }

    public Queue<Call> getCallQueue() {
        return callQueue;
    }

    public void setCallQueue(Queue<Call> callQueue) {
        this.callQueue = callQueue;
    }

    public void addAgent(Employee employee) {
        if (this.agentList == null) agentList = new ArrayList<Employee>();
        agentList.add(employee);
    }

    public interface ITryToHandleCall{
        void tryToHandleCall(Role role);
    }

    public List<Call> getAnsweredCalls() {
        return answeredCalls;
    }

    public void setAnsweredCalls(List<Call> answeredCalls) {
        this.answeredCalls = answeredCalls;
    }

    public List<Employee> getAllEmployee() {
        if (agentList == null) agentList = new ArrayList<Employee>();
        if (manager != null) agentList.add(manager);
        if (supervisor != null) agentList.add(supervisor);

        return agentList;
    }

}



