package com.testwork.callcenter.models;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by renat on 21.03.16.
 */
public enum  Role {
    AGENT,
    MANAGER,
    SUPERVISOR;

    private static final List<Role> VALUES =
            Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static Role randomRole()  {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
