package com.testwork.callcenter.models;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by renat on 21.03.16.
 */
public class Employee {
    private long id;
    private String name;
    private Call call;
    private Status status;
    private Company.ITryToHandleCall iTryToHandleCall;
    private int receivedCallsCount;
    private int summDuration;
    private Role role;

    public Employee() {
    }

    public Employee(long id, String name, Company.ITryToHandleCall iTryToHandleCall) {
        this.id = id;
        this.name = name;
        this.status = Status.FREE;
        this.iTryToHandleCall = iTryToHandleCall;
    }

    public Employee(long id, String name, Company.ITryToHandleCall iTryToHandleCall, Role role) {
        this.id = id;
        this.name = name;
        this.status = Status.FREE;
        this.iTryToHandleCall = iTryToHandleCall;
        this.role = role;
    }

    public Employee handle(Call call) {
        this.status = Status.BUSY;
        this.receivedCallsCount++;
        this.call = call;
        summDuration+=call.getDuration();

        final Employee current = this;
        call.setAnswered(Boolean.TRUE);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                current.broke();
            }
        }, call.getDuration());
        return this;
    }

    public Employee broke() {
        System.out.println("Employee is free ------ " +this.getName());
        this.status = Status.FREE;
        Role role = call.getRole();
        this.call = null;
        if (iTryToHandleCall != null) {
            iTryToHandleCall.tryToHandleCall(role);
        }
        return this;
    }

    public Boolean canHandle() {
        return this.status == Status.FREE;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public enum Status{
        FREE, BUSY
    }

    public int getSummDuration() {
        return summDuration;
    }

    public void setSummDuration(int summDuration) {
        this.summDuration = summDuration;
    }

    public int getReceivedCallsCount() {
        return receivedCallsCount;
    }

    public void setReceivedCallsCount(int receivedCallsCount) {
        this.receivedCallsCount = receivedCallsCount;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", call=" + call +
                ", status=" + status +
                ", receivedCallsCount=" + receivedCallsCount +
                ", summDuration=" + summDuration +
                '}';
    }
}
